//Diesen import nicht großartig beachten
import Notify from './Notify'
//Operatoren import
import Operator from './operators/operator';

import Plus, {PLUS_OPERATOR_CHAR} from './operators/plus';
import Minus, {MINUS_OPERATOR_CHAR} from './operators/minus';
import Root, {ROOT_OPERATOR_CHAR} from './operators/root';
import Multiply, {MULTIPLY_OPERATOR_CHAR} from './operators/multiply';
import Power, {POW_OPERATOR_CHAR} from './operators/pow';
import Divison, {DIVISION_OPERATOR_CHAR} from './operators/division';

export default class Main {
    output:HTMLElement;
    input:HTMLElement;
    operators: Map<string, Operator>
    constructor(){
        this.output = document.getElementById("calculator-output");
        this.input = document.getElementById("calculator-input");
        this.operators = new Map();
        this.operators.set(PLUS_OPERATOR_CHAR, new Plus());
        this.operators.set(MINUS_OPERATOR_CHAR, new Minus());
        this.operators.set(ROOT_OPERATOR_CHAR, new Root());
        this.operators.set(MULTIPLY_OPERATOR_CHAR, new Multiply());
        this.operators.set(POW_OPERATOR_CHAR, new Power());
        this.operators.set(DIVISION_OPERATOR_CHAR, new Divison());
        
        this.registerKeyEvents();
    }

    registerKeyEvents ():void {
        window.addEventListener("keyup", this.getKeys, false)

        let btns = document.getElementsByClassName("calc-btn");
        for(let i = 0; i < btns.length; i++){
            btns[i].addEventListener("click", this.btnClick, false);
        }
    }

    btnClick = (e:Event) => {
        this.getKeys(e.srcElement.innerHTML);
    }

    getKeys = (e:KeyboardEvent|string):void => {
        let keyname:string = null;
        if(e instanceof KeyboardEvent)
            keyname = e.key;
        else 
            keyname = e;
            
        switch(keyname){
            case "Enter":
            case "=":
                this.calculate();
                return;
            case "Backspace":
                this.removeChar();
                return;
            case "Dead": // Keyname der Taste "^" ist "Dead", daher der workaround hier.
                keyname = "^";break; 
            default:
                break;
        }
        
        //Prüfen ob eingegebenes Zeichen eine Zahl ist.
        if(parseInt(keyname) >= 0 && parseInt(keyname) <= 9){
            this.printKey(keyname);
            return;
        }

        //Prüfen ob letztes Zeichen eine Nummer war.
        if(parseInt(this.input.innerHTML[ this.input.innerHTML.length-1])){
            this.operators.forEach((op, key)=> {
                if(op.isKey(keyname)){
                    this.printKey(op.operatorChar);
                    return;
                }
            })
        }
    }

    printKey(keyName:string){
        this.input.innerHTML += keyName;
    }

    removeChar() {
        let strIn = this.input.innerHTML;
        this.input.innerHTML = strIn.substring(0, strIn.length -1);
    }

    calculate() {
        let numbers:Array<string> = [];
        let opOrder:Array<Operator> = [];
        let curr = 0;
        let strIn = this.input.innerHTML;
        for (let i = 0; i < strIn.length; i++) {
            let char = strIn.charAt(i);
            if(parseInt(char) || char == '0'){
                if(numbers[curr])
                    numbers[curr] += char;
                else
                    numbers[curr] = char;
            }
            else {
                let op = this.operators.get(char);
                if(op){
                    opOrder[curr] = op;
                    curr++;
                }
                else{
                    new Notify("error", "Kein passender Operator gefunden.");
                    return;
                }
            }
        }

        // Array umdrehen damit erstes element hinten steht (wegen pop())
        numbers = numbers.reverse();
        let res:number = 0;
        if(numbers[0])
            res = parseInt(numbers.pop());

        opOrder.forEach((element) => {
            try {
                let b = parseInt(numbers.pop());
                res = element.calculate(res, b);
            } catch(err){
                new Notify("error", "Fehler beim rechnen: "+err);
                return;
            }
        });
      
        this.output.innerHTML = res.toString();
        this.input.innerHTML = "";        
    }
}

let main = null;

main = new Main();