import Operator from './operator';
export const PLUS_OPERATOR_CHAR = '+';
export default class Plus implements Operator {
    operatorChar: string = PLUS_OPERATOR_CHAR;
    calculate(left :number, right:number):number {
        return left + right;
    }
    isKey(keyname:string){
        return (keyname == this.operatorChar)
    }
}