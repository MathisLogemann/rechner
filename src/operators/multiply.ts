import Operator from './operator';
export const MULTIPLY_OPERATOR_CHAR:string = '*';

export default class Multiply implements Operator {
    operatorChar: string = MULTIPLY_OPERATOR_CHAR;

    calculate(left :number, right:number):number {
        return left * right;
    }

    isKey(keyname:string){
        return keyname == this.operatorChar;
    }
}