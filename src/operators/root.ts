import Operator from './operator';
export const ROOT_OPERATOR_CHAR = 'w';

export default class Root implements Operator {
    operatorChar: string = ROOT_OPERATOR_CHAR;

    calculate(exponent :number = 2, radikant:number):number {
        if(radikant >= 0){
            return Math.pow(radikant, 1/exponent);
        }
        else
            throw "Ungültig";
    }

    isKey(keyname:string){
        return keyname == this.operatorChar;
    }
}