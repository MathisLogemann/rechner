import Operator from './operator';
export const POW_OPERATOR_CHAR:string = '^';

export default class Power implements Operator {
    operatorChar: string = POW_OPERATOR_CHAR;

    calculate(left :number, right:number):number {
        return Math.pow(left, right);
    }

    isKey(keyname:string){
        return keyname == this.operatorChar;
    }

}
