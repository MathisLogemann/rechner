import Operator from './operator';
export const MINUS_OPERATOR_CHAR:string = '-';

export default class Minus implements Operator {
    operatorChar: string = MINUS_OPERATOR_CHAR;

    calculate(left :number, right:number):number {
        return left - right;
    }

    isKey(keyname:string){
        return keyname == this.operatorChar;
    }
}