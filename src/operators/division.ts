import Operator from './operator';
export const DIVISION_OPERATOR_CHAR:string = '/';

export default class Divison implements Operator {
    operatorChar: string = DIVISION_OPERATOR_CHAR;

    calculate(left :number, right:number):number {
        if(right == 0) throw "Nicht durch Null teilen!";
        return left / right;
    }

    isKey(keyname:string){
        return keyname == this.operatorChar;
    }

}
