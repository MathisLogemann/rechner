export default interface Operator {
    //Das Zeichen welches für den Operator verwendet werden soll.
    operatorChar:string;
    //Die Berechnungsmethode
    calculate(a: number, b:number, c?:number): number;
    //Soll prüfen, ob der keyname mit dem operatorChar übereinstimmt.
    isKey(keyname:string):boolean;
}