export default class Notify {
    status:string;
    element:HTMLDivElement;
    text:string;
    constructor(status:"success"|"error"|"warning", text:string, autoShow:boolean = true, autoHide:boolean = true, durationUntilHide:number = 5000){
        this.status = status;
        this.text = text;
        if(autoShow){
            this.show();
        }
        if(autoHide){
            setTimeout(this.destroy, durationUntilHide);
        }
    }

    show(){
        this.element = document.createElement("div");
        this.element.classList.add("toast", "toast-"+this.status);

        let btn = document.createElement("button");
        btn.classList.add("btn", "btn-clear", "float-right");
        btn.addEventListener("click", this.destroy, false);
        this.element.appendChild(btn);

        this.element.insertAdjacentHTML("beforeend", this.text);
        document.body.insertBefore(this.element, document.body.firstChild);
    }

    destroy = () => {
        this.element.remove();
    }
}