"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MULTIPLY_OPERATOR_CHAR = '*';
class Multiply {
    constructor() {
        this.operatorChar = exports.MULTIPLY_OPERATOR_CHAR;
    }
    calculate(left, right) {
        return left * right;
    }
    isKey(keyname) {
        return keyname == this.operatorChar;
    }
}
exports.default = Multiply;
//# sourceMappingURL=multiply.js.map