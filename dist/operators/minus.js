"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MINUS_OPERATOR_CHAR = '-';
class Minus {
    constructor() {
        this.operatorChar = exports.MINUS_OPERATOR_CHAR;
    }
    calculate(left, right) {
        return left - right;
    }
    isKey(keyname) {
        return keyname == this.operatorChar;
    }
}
exports.default = Minus;
//# sourceMappingURL=minus.js.map