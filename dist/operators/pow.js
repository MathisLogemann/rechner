"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.POW_OPERATOR_CHAR = '^';
class Power {
    constructor() {
        this.operatorChar = exports.POW_OPERATOR_CHAR;
    }
    calculate(left, right) {
        return Math.pow(left, right);
    }
    isKey(keyname) {
        return keyname == this.operatorChar;
    }
}
exports.default = Power;
//# sourceMappingURL=pow.js.map