"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLUS_OPERATOR_CHAR = '+';
class Plus {
    constructor() {
        this.operatorChar = exports.PLUS_OPERATOR_CHAR;
    }
    calculate(left, right) {
        return left + right;
    }
    isKey(keyname) {
        return (keyname == this.operatorChar);
    }
}
exports.default = Plus;
//# sourceMappingURL=plus.js.map