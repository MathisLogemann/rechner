"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DIVISION_OPERATOR_CHAR = '/';
class Divison {
    constructor() {
        this.operatorChar = exports.DIVISION_OPERATOR_CHAR;
    }
    calculate(left, right) {
        if (right == 0)
            throw "Nicht durch Null teilen!";
        return left / right;
    }
    isKey(keyname) {
        return keyname == this.operatorChar;
    }
}
exports.default = Divison;
//# sourceMappingURL=division.js.map