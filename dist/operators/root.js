"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROOT_OPERATOR_CHAR = 'w';
class Root {
    constructor() {
        this.operatorChar = exports.ROOT_OPERATOR_CHAR;
    }
    calculate(exponent = 2, radikant) {
        if (radikant >= 0) {
            return Math.pow(radikant, 1 / exponent);
        }
        else
            throw "Ungültig";
    }
    isKey(keyname) {
        return keyname == this.operatorChar;
    }
}
exports.default = Root;
//# sourceMappingURL=root.js.map