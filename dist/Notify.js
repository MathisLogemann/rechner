"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Notify {
    constructor(status, text, autoShow = true, autoHide = true, durationUntilHide = 5000) {
        this.destroy = () => {
            this.element.remove();
        };
        this.status = status;
        this.text = text;
        if (autoShow) {
            this.show();
        }
        if (autoHide) {
            setTimeout(this.destroy, durationUntilHide);
        }
    }
    show() {
        this.element = document.createElement("div");
        this.element.classList.add("toast", "toast-" + this.status);
        let btn = document.createElement("button");
        btn.classList.add("btn", "btn-clear", "float-right");
        btn.addEventListener("click", this.destroy, false);
        this.element.appendChild(btn);
        this.element.insertAdjacentHTML("beforeend", this.text);
        document.body.insertBefore(this.element, document.body.firstChild);
    }
}
exports.default = Notify;
//# sourceMappingURL=Notify.js.map