"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//Diesen import nicht großartig beachten
const Notify_1 = require("./Notify");
const plus_1 = require("./operators/plus");
const minus_1 = require("./operators/minus");
const root_1 = require("./operators/root");
const multiply_1 = require("./operators/multiply");
const pow_1 = require("./operators/pow");
const division_1 = require("./operators/division");
class Main {
    constructor() {
        this.btnClick = (e) => {
            this.getKeys(e.srcElement.innerHTML);
        };
        this.getKeys = (e) => {
            let keyname = null;
            if (e instanceof KeyboardEvent)
                keyname = e.key;
            else
                keyname = e;
            switch (keyname) {
                case "Enter":
                case "=":
                    this.calculate();
                    return;
                case "Backspace":
                    this.removeChar();
                    return;
                case "Dead":// Keyname der Taste "^" ist "Dead", daher der workaround hier.
                    keyname = "^";
                    break;
                default:
                    break;
            }
            if (parseInt(keyname) >= 0 && parseInt(keyname) <= 9) {
                this.printKey(keyname);
                return;
            }
            if (parseInt(this.input.innerHTML[this.input.innerHTML.length - 1])) {
                this.operators.forEach((op, key) => {
                    if (op.isKey(keyname)) {
                        this.printKey(op.operatorChar);
                        return;
                    }
                });
            }
        };
        this.output = document.getElementById("calculator-output");
        this.input = document.getElementById("calculator-input");
        this.operators = new Map();
        this.operators.set(plus_1.PLUS_OPERATOR_CHAR, new plus_1.default());
        this.operators.set(minus_1.MINUS_OPERATOR_CHAR, new minus_1.default());
        this.operators.set(root_1.ROOT_OPERATOR_CHAR, new root_1.default());
        this.operators.set(multiply_1.MULTIPLY_OPERATOR_CHAR, new multiply_1.default());
        this.operators.set(pow_1.POW_OPERATOR_CHAR, new pow_1.default());
        this.operators.set(division_1.DIVISION_OPERATOR_CHAR, new division_1.default());
        this.registerKeyEvents();
    }
    registerKeyEvents() {
        window.addEventListener("keyup", this.getKeys, false);
        let btns = document.getElementsByClassName("calc-btn");
        for (let i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", this.btnClick, false);
        }
    }
    printKey(keyName) {
        this.input.innerHTML += keyName;
    }
    removeChar() {
        let strIn = this.input.innerHTML;
        this.input.innerHTML = strIn.substring(0, strIn.length - 1);
    }
    calculate() {
        let numbers = [];
        let opOrder = [];
        let curr = 0;
        let strIn = this.input.innerHTML;
        for (let i = 0; i < strIn.length; i++) {
            let char = strIn.charAt(i);
            if (parseInt(char) || char == '0') {
                if (numbers[curr])
                    numbers[curr] += char;
                else
                    numbers[curr] = char;
            }
            else {
                let op = this.operators.get(char);
                if (op) {
                    opOrder[curr] = op;
                    curr++;
                }
                else {
                    new Notify_1.default("error", "Kein passender Operator gefunden.");
                    return;
                }
            }
        }
        numbers = numbers.reverse();
        let res = 0;
        if (numbers[0])
            res = parseInt(numbers.pop());
        opOrder.forEach((element) => {
            try {
                let b = parseInt(numbers.pop());
                res = element.calculate(res, b);
            }
            catch (err) {
                new Notify_1.default("error", "Fehler beim rechnen: " + err);
                return;
            }
        });
        this.output.innerHTML = res.toString();
        this.input.innerHTML = "";
    }
}
exports.default = Main;
let main = null;
main = new Main();
//# sourceMappingURL=main.js.map