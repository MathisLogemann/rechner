/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
//Diesen import nicht großartig beachten
var Notify_1 = __webpack_require__(1);
var plus_1 = __webpack_require__(2);
var minus_1 = __webpack_require__(3);
var root_1 = __webpack_require__(4);
var multiply_1 = __webpack_require__(5);
var pow_1 = __webpack_require__(6);
var division_1 = __webpack_require__(7);

var Main = function () {
    function Main() {
        var _this = this;

        _classCallCheck(this, Main);

        this.btnClick = function (e) {
            _this.getKeys(e.srcElement.innerHTML);
        };
        this.getKeys = function (e) {
            var keyname = null;
            if (e instanceof KeyboardEvent) keyname = e.key;else keyname = e;
            switch (keyname) {
                case "Enter":
                case "=":
                    _this.calculate();
                    return;
                case "Backspace":
                    _this.removeChar();
                    return;
                case "Dead":
                    // Keyname der Taste "^" ist "Dead", daher der workaround hier.
                    keyname = "^";
                    break;
                default:
                    break;
            }
            if (parseInt(keyname) >= 0 && parseInt(keyname) <= 9) {
                _this.printKey(keyname);
                return;
            }
            if (parseInt(_this.input.innerHTML[_this.input.innerHTML.length - 1])) {
                _this.operators.forEach(function (op, key) {
                    if (op.isKey(keyname)) {
                        _this.printKey(op.operatorChar);
                        return;
                    }
                });
            }
        };
        this.output = document.getElementById("calculator-output");
        this.input = document.getElementById("calculator-input");
        this.operators = new Map();
        this.operators.set(plus_1.PLUS_OPERATOR_CHAR, new plus_1.default());
        this.operators.set(minus_1.MINUS_OPERATOR_CHAR, new minus_1.default());
        this.operators.set(root_1.ROOT_OPERATOR_CHAR, new root_1.default());
        this.operators.set(multiply_1.MULTIPLY_OPERATOR_CHAR, new multiply_1.default());
        this.operators.set(pow_1.POW_OPERATOR_CHAR, new pow_1.default());
        this.operators.set(division_1.DIVISION_OPERATOR_CHAR, new division_1.default());
        this.registerKeyEvents();
    }

    _createClass(Main, [{
        key: "registerKeyEvents",
        value: function registerKeyEvents() {
            window.addEventListener("keyup", this.getKeys, false);
            var btns = document.getElementsByClassName("calc-btn");
            for (var i = 0; i < btns.length; i++) {
                btns[i].addEventListener("click", this.btnClick, false);
            }
        }
    }, {
        key: "printKey",
        value: function printKey(keyName) {
            this.input.innerHTML += keyName;
        }
    }, {
        key: "removeChar",
        value: function removeChar() {
            var strIn = this.input.innerHTML;
            this.input.innerHTML = strIn.substring(0, strIn.length - 1);
        }
    }, {
        key: "calculate",
        value: function calculate() {
            var numbers = [];
            var opOrder = [];
            var curr = 0;
            var strIn = this.input.innerHTML;
            for (var i = 0; i < strIn.length; i++) {
                var char = strIn.charAt(i);
                if (parseInt(char) || char == '0') {
                    if (numbers[curr]) numbers[curr] += char;else numbers[curr] = char;
                } else {
                    var op = this.operators.get(char);
                    if (op) {
                        opOrder[curr] = op;
                        curr++;
                    } else {
                        new Notify_1.default("error", "Kein passender Operator gefunden.");
                        return;
                    }
                }
            }
            numbers = numbers.reverse();
            var res = 0;
            if (numbers[0]) res = parseInt(numbers.pop());
            opOrder.forEach(function (element) {
                try {
                    var b = parseInt(numbers.pop());
                    res = element.calculate(res, b);
                } catch (err) {
                    new Notify_1.default("error", "Fehler beim rechnen: " + err);
                    return;
                }
            });
            this.output.innerHTML = res.toString();
            this.input.innerHTML = "";
        }
    }]);

    return Main;
}();

exports.default = Main;
var main = null;
main = new Main();
//# sourceMappingURL=main.js.map

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var Notify = function () {
    function Notify(status, text) {
        var autoShow = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

        var _this = this;

        var autoHide = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
        var durationUntilHide = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 5000;

        _classCallCheck(this, Notify);

        this.destroy = function () {
            _this.element.remove();
        };
        this.status = status;
        this.text = text;
        if (autoShow) {
            this.show();
        }
        if (autoHide) {
            setTimeout(this.destroy, durationUntilHide);
        }
    }

    _createClass(Notify, [{
        key: "show",
        value: function show() {
            this.element = document.createElement("div");
            this.element.classList.add("toast", "toast-" + this.status);
            var btn = document.createElement("button");
            btn.classList.add("btn", "btn-clear", "float-right");
            btn.addEventListener("click", this.destroy, false);
            this.element.appendChild(btn);
            this.element.insertAdjacentHTML("beforeend", this.text);
            document.body.insertBefore(this.element, document.body.firstChild);
        }
    }]);

    return Notify;
}();

exports.default = Notify;
//# sourceMappingURL=Notify.js.map

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
exports.PLUS_OPERATOR_CHAR = '+';

var Plus = function () {
    function Plus() {
        _classCallCheck(this, Plus);

        this.operatorChar = exports.PLUS_OPERATOR_CHAR;
    }

    _createClass(Plus, [{
        key: "calculate",
        value: function calculate(left, right) {
            return left + right;
        }
    }, {
        key: "isKey",
        value: function isKey(keyname) {
            return keyname == this.operatorChar;
        }
    }]);

    return Plus;
}();

exports.default = Plus;
//# sourceMappingURL=plus.js.map

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
exports.MINUS_OPERATOR_CHAR = '-';

var Minus = function () {
    function Minus() {
        _classCallCheck(this, Minus);

        this.operatorChar = exports.MINUS_OPERATOR_CHAR;
    }

    _createClass(Minus, [{
        key: "calculate",
        value: function calculate(left, right) {
            return left - right;
        }
    }, {
        key: "isKey",
        value: function isKey(keyname) {
            return keyname == this.operatorChar;
        }
    }]);

    return Minus;
}();

exports.default = Minus;
//# sourceMappingURL=minus.js.map

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
exports.ROOT_OPERATOR_CHAR = 'w';

var Root = function () {
    function Root() {
        _classCallCheck(this, Root);

        this.operatorChar = exports.ROOT_OPERATOR_CHAR;
    }

    _createClass(Root, [{
        key: "calculate",
        value: function calculate() {
            var exponent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 2;
            var radikant = arguments[1];

            if (radikant >= 0) {
                return Math.pow(radikant, 1 / exponent);
            } else throw "Ungültig";
        }
    }, {
        key: "isKey",
        value: function isKey(keyname) {
            return keyname == this.operatorChar;
        }
    }]);

    return Root;
}();

exports.default = Root;
//# sourceMappingURL=root.js.map

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
exports.MULTIPLY_OPERATOR_CHAR = '*';

var Multiply = function () {
    function Multiply() {
        _classCallCheck(this, Multiply);

        this.operatorChar = exports.MULTIPLY_OPERATOR_CHAR;
    }

    _createClass(Multiply, [{
        key: "calculate",
        value: function calculate(left, right) {
            return left * right;
        }
    }, {
        key: "isKey",
        value: function isKey(keyname) {
            return keyname == this.operatorChar;
        }
    }]);

    return Multiply;
}();

exports.default = Multiply;
//# sourceMappingURL=multiply.js.map

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
exports.POW_OPERATOR_CHAR = '^';

var Power = function () {
    function Power() {
        _classCallCheck(this, Power);

        this.operatorChar = exports.POW_OPERATOR_CHAR;
    }

    _createClass(Power, [{
        key: "calculate",
        value: function calculate(left, right) {
            return Math.pow(left, right);
        }
    }, {
        key: "isKey",
        value: function isKey(keyname) {
            return keyname == this.operatorChar;
        }
    }]);

    return Power;
}();

exports.default = Power;
//# sourceMappingURL=pow.js.map

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
exports.DIVISION_OPERATOR_CHAR = '/';

var Divison = function () {
    function Divison() {
        _classCallCheck(this, Divison);

        this.operatorChar = exports.DIVISION_OPERATOR_CHAR;
    }

    _createClass(Divison, [{
        key: "calculate",
        value: function calculate(left, right) {
            if (right == 0) throw "Nicht durch Null teilen!";
            return left / right;
        }
    }, {
        key: "isKey",
        value: function isKey(keyname) {
            return keyname == this.operatorChar;
        }
    }]);

    return Divison;
}();

exports.default = Divison;
//# sourceMappingURL=division.js.map

/***/ })
/******/ ]);
//# sourceMappingURL=main.bundle.js.map