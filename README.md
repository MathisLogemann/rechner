# README #

- git clone https://MathisLogemann@bitbucket.org/MathisLogemann/rechner.git --branch demo
- cd rechner
- npm install
- npm run start

# Änderungen
- npm run tsc (wenn an typescript etwas veränder worden ist)
- npm run webpack (wenn nur etwas an dem es6 code geändert worden ist)
## Achtung
- npm run tsc überschreibt den es6 code! 

## Nicht die Demo Branch? 
* git checkout demo